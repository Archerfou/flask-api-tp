FROM python:3.7.13-bullseye

RUN mkdir /app
COPY . /app
WORKDIR /app/
RUN pip install -r requirements.txt
RUN pip install pytest

CMD ["pytest"]
